-- Read the docs: https://www.lunarvim.org/docs/configuration
-- Video Tutorials: https://www.youtube.com/watch?v=sFA9kX-Ud_c&list=PLhoH5vyxr6QqGu0i7tt_XoVK9v-KvZ3m6
-- Forum: https://www.reddit.com/r/lunarvim/
-- Discord: https://discord.com/invite/Xb9B4Ny

--[[
lvim is the global options object

Linters should be
filled in as strings with either
a global executable or a path to
an executable

]]
-- THESE ARE EXAMPLE CONFIGS FEEL FREE TO CHANGE TO WHATEVER YOU WANT


-- general
lvim.log.level = "warn"
lvim.format_on_save = false
lvim.colorscheme = "onedarker"
-- lvim.colorscheme = "delek"
-- lvim.colorscheme = "material"
-- vim.g.material_style = "oceanic"

-- to disable icons and use a minimalist setup, uncomment the following
-- lvim.use_icons = false

-- keymappings [view all the defaults by pressing <leader>Lk]
lvim.leader = "space"
-- add your own keymapping
lvim.keys.normal_mode["<C-s>"] = ":w<cr>"
lvim.keys.normal_mode["<A-.>"] = ":BufferLineCycleNext<CR>"
lvim.keys.normal_mode["<A-,>"] = ":BufferLineCyclePrev<CR>"
-- unmap a default keymapping
-- vim.keymap.del("n", "<C-Up>")
-- override a default keymapping
-- lvim.keys.normal_mode["<C-q>"] = ":q<cr>" -- or vim.keymap.set("n", "<C-q>", ":q<cr>" )

-- Do not swap lines when exiting from insert mode
lvim.keys.insert_mode["<A-j>"] = false
lvim.keys.insert_mode["<A-k>"] = false
lvim.keys.normal_mode["<A-j>"] = false
lvim.keys.normal_mode["<A-k>"] = false
lvim.keys.visual_block_mode["<A-j>"] = false
lvim.keys.visual_block_mode["<A-k>"] = false
lvim.keys.visual_block_mode["J"] = false
lvim.keys.visual_block_mode["K"] = false

-- Change Telescope navigation to use j and k for navigation and n and p for history in both input and normal mode.
-- we use protected-mode (pcall) just in case the plugin wasn't loaded yet.
-- local _, actions = pcall(require, "telescope.actions")
-- lvim.builtin.telescope.defaults.mappings = {
--   -- for input mode
--   i = {
--     ["<C-j>"] = actions.move_selection_next,
--     ["<C-k>"] = actions.move_selection_previous,
--     ["<C-n>"] = actions.cycle_history_next,
--     ["<C-p>"] = actions.cycle_history_prev,
--   },
--   -- for normal mode
--   n = {
--     ["<C-j>"] = actions.move_selection_next,
--     ["<C-k>"] = actions.move_selection_previous,
--   },
-- }

-- Change theme settings
-- lvim.builtin.theme.options.dim_inactive = true
-- lvim.builtin.theme.options.style = "storm"

-- Use which-key to add extra bindings with the leader-key prefix
-- lvim.builtin.which_key.mappings["P"] = { "<cmd>Telescope projects<CR>", "Projects" }
-- lvim.builtin.which_key.mappings["t"] = {
--   name = "+Trouble",
--   r = { "<cmd>Trouble lsp_references<cr>", "References" },
--   f = { "<cmd>Trouble lsp_definitions<cr>", "Definitions" },
--   d = { "<cmd>Trouble document_diagnostics<cr>", "Diagnostics" },
--   q = { "<cmd>Trouble quickfix<cr>", "QuickFix" },
--   l = { "<cmd>Trouble loclist<cr>", "LocationList" },
--   w = { "<cmd>Trouble workspace_diagnostics<cr>", "Workspace Diagnostics" },
-- }

-- TODO: User Config for predefined plugins
-- After changing plugin config exit and reopen LunarVim, Run :PackerInstall :PackerCompile
lvim.builtin.alpha.active = true
lvim.builtin.alpha.mode = "dashboard"
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.setup.actions.open_file.resize_window = true
lvim.builtin.nvimtree.setup.renderer.icons.show.git = false

-- if you don't want all the parsers change this to a table of the ones you want
lvim.builtin.treesitter.ensure_installed = {
  "bash",
  "c",
  "javascript",
  "json",
  "lua",
  "python",
  "typescript",
  "tsx",
  "css",
  "rust",
  "java",
  "yaml",
}

lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.highlight.enable = true

-- generic LSP settings

-- make sure server will always be installed even if the server is in skipped_servers list
lvim.lsp.installer.setup.ensure_installed = {
    -- "pyright",
    "ruff_lsp",
    "lua_ls",
    "jsonls",
}
-- -- change UI setting of `LspInstallInfo`
-- -- see <https://github.com/williamboman/nvim-lsp-installer#default-configuration>
-- lvim.lsp.installer.setup.ui.check_outdated_servers_on_open = false
-- lvim.lsp.installer.setup.ui.border = "rounded"
-- lvim.lsp.installer.setup.ui.keymaps = {
--     uninstall_server = "d",
--     toggle_server_expand = "o",
-- }

-- ---@usage disable automatic installation of servers
-- lvim.lsp.installer.setup.automatic_installation = false

-- ---configure a server manually. !!Requires `:LvimCacheReset` to take effect!!
-- ---see the full default list `:lua print(vim.inspect(lvim.lsp.automatic_configuration.skipped_servers))`
-- vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "pyright" })
-- local opts = {} -- check the lspconfig documentation for a list of all possible options
-- require("lvim.lsp.manager").setup("pyright", opts)

-- ---remove a server from the skipped list, e.g. eslint, or emmet_ls. !!Requires `:LvimCacheReset` to take effect!!
-- ---`:LvimInfo` lists which server(s) are skipped for the current filetype
-- lvim.lsp.automatic_configuration.skipped_servers = vim.tbl_filter(function(server)
--   return server ~= "emmet_ls"
-- end, lvim.lsp.automatic_configuration.skipped_servers)

-- -- you can set a custom on_attach function that will be used for all the language servers
-- -- See <https://github.com/neovim/nvim-lspconfig#keybindings-and-completion>
-- lvim.lsp.on_attach_callback = function(client, bufnr)
--   local function buf_set_option(...)
--     vim.api.nvim_buf_set_option(bufnr, ...)
--   end
--   --Enable completion triggered by <c-x><c-o>
--   buf_set_option("omnifunc", "v:lua.vim.lsp.omnifunc")
-- end

-- -- set a formatter, this will override the language server formatting capabilities (if it exists)
local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
  { command = "black", filetypes = { "python" } },
  { command = "isort", filetypes = { "python" } },
--   {
--     -- each formatter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
--     command = "prettier",
--     ---@usage arguments to pass to the formatter
--     -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
--     extra_args = { "--print-with", "100" },
--     ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
--     filetypes = { "typescript", "typescriptreact" },
--   },
}


-- Custom config
-- Override settings here from  /home/niekas/.local/share/lunarvim/lvim/lua/lvim/config/settings.lua
-- vim.opt["timeoutlen"] = 1500 -- time to wait for a mapped sequence to complete (in milliseconds)
vim.opt["mouse"] = "iv" -- modes in which mouse will work (disabled mouse to be able to select in term mode)
vim.opt["tabstop"] = 4
vim.opt["shiftwidth"] = 4
vim.opt["smartcase"] = false
vim.opt["smartindent"] = false
vim.opt["ignorecase"] = true


-- add your own keymapping
lvim.keys.normal_mode["<C-s>"] = ":w<cr>"
lvim.keys.normal_mode["<C-x>"] = ":x<cr>"

lvim.keys.normal_mode["<C-q>"] = ":q<cr>"

lvim.keys.normal_mode["_"] = "@q"

lvim.keys.normal_mode["<A-a>"] = "ea"
lvim.keys.normal_mode["<A-A>"] = "Ea"
lvim.keys.normal_mode["<A-i>"] = "bi"
lvim.keys.normal_mode["<A-I>"] = "Bi"
-- lvim.keys.normal_mode["\""] = "ysiW\""

lvim.keys.normal_mode["<leader>t"] = ":w<CR>:call RunTest()<CR>"
lvim.keys.normal_mode["<leader>i"] = ":call InsertBreakpoint()<CR>:w<CR>"

-- lvim.keys.normal_mode["<leader>b"]  = ":call CreateJIRABranchFromRegister()<CR>"
lvim.keys.normal_mode["<leader>a"]  = "O<esc>:.! date '+\\%Y-\\%m-\\%d'<Enter>A[]<esc>hx^P<CR>"
lvim.keys.normal_mode["<leader>da"] = "GVggxi"


-- Additional Plugins
lvim.plugins = {
    { 'marko-cerovac/material.nvim' },
    { "lervag/file-line" }, -- Open files in given line
    { "tpope/vim-unimpaired" },
    { "tpope/vim-surround" },
    { "tpope/vim-repeat" }, -- ysiw" or "" surounds word with brackets
    { "arthurxavierx/vim-caser" }, -- gs- gsU gs_ - changes word caption type
    { "inkarkat/vim-SyntaxRange" },
    { "airblade/vim-matchquote" }, -- match quotes with % sign
    { "andymass/vim-matchup" },

    { "felipec/vim-sanegx" }, -- fixes open urls in browser with gx shortcut
    -- { "glacambre/firenvim",
    --     build = function() vim.fn['firenvim#install'](0) end
    -- },  -- call firenvim#install(0)
    -- TODO setup debuging:
    -- { "mfussenegger/nvim-dap" },
    -- { "mfussenegger/nvim-dap-python" },
    -- { "rcarriga/nvim-dap-ui" },
    -- { "theHamsta/nvim-dap-virtual-text" },
    -- { "nvim-telescope/telescope-dap.nvim" },

    -- pip install neovim debugpy

    --     {"folke/tokyonight.nvim"},
    --     {
    --       "folke/trouble.nvim",
    --       cmd = "TroubleToggle",
    --     },
}
vim.g.file_line_crosshairs = 0

-- For some reason this mapping does not work
-- lvim.keys.normal_mode[";"] = "ysiw\""

-- Autocommands (https://neovim.io/doc/user/autocmd.html)
vim.api.nvim_create_autocmd("TermOpen", {
    pattern = { "*" },
    command = "startinsert",
})


-- TODO:
-- GG should not popup table of selctions
-- This table of selection should be created automatically
-- When writing comments no suggestions should be shown
-- How to add basic vim configuration commands?
-- Wrong messages should not be rendered dirrectly to the line (or at least the color should be different).
-- How to add
-- Search term should be no longer highlighted after another command is executed.


-- change project fast
vim.keymap.set('n', '[[', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']]', vim.diagnostic.goto_next)

-- from https://www.youtube.com/watch?v=0moS8UHupGc&t=430s
vim.keymap.set('n', '<F5>', ':lua require"dap".continue()<CR>')
vim.keymap.set('n', '<F10>', ':lua require"dap".step_over()<CR>')
vim.keymap.set('n', '<F11>', ':lua require"dap".step_into()<CR>')
vim.keymap.set('n', '<F12>', ':lua require"dap".step_out()<CR>')
vim.keymap.set('n', '<leader>b', ':lua require"dap".toggle_breakpoint()<CR>')
vim.keymap.set('n', '<leader>B', ':lua require"dap".set_breakpoint(vim.fn.input("Breakpoint condition: "))<CR>')
vim.keymap.set('n', '<leader>lp', ':lua require"dap".set_breakpoint(nil, nil, vim.fn.input("Log point message: "))<CR>')
vim.keymap.set('n', '<leader>dr', ':lua require"dap".repl.open()<CR>')


local parser_configs = require("nvim-treesitter.parsers").get_parser_configs()
parser_configs.hcl = {
    filetype = "hcl", "terraform",
}

--- :%s/\s\+$//  -- removes all trailing spaces



-- Functions for interacting with diagnostics:
function _G.disable_diagnostics()
  vim.diagnostic.enable(false)
end

function _G.enable_diagnostics()
  vim.diagnostic.enable(true)
end


-- TODO: use this plugin for highlighting vimrc and python correctly: https://github.com/inkarkat/vim-SyntaxRange
vim.cmd([[
set rnu

map        \    gcc
vmap       \    gc
" nmap       ""   ysiW"
nmap       ""w   ysiw"
nmap       ""W   ysiW"
vmap       ""   S"
map        <leader>y    "*y
vmap       <leader>y    "*y
map        <leader>p    "*p
map        <leader>P    "*P

nmap       _            @q
nmap       <leader>t    :w<CR>:call RunTest()<CR>
nmap       <leader>i    :call InsertBreakpoint()<CR>:w<CR>
nmap       <leader>a    O<esc>:.! date '+\\%Y-\\%m-\\%d'<Enter>A[]<esc>hx^P<CR>
nmap       <leader>da   GVggxi


function! RunTest()
python3 << EOF
# CallMakeTestWithCurrentPythonTest()
# TODO: Should allow to somehow run pytest without arguments
import re
import os
import vim  # https://vimhelp.org/if_pyth.txt.html
import pynvim

cursor = vim.current.window.cursor
test_filename = vim.eval("expand('%p')")
if os.path.basename(test_filename).startswith('test'):
    test_name = None
    class_name = None
    # " Parse test name and its TestCase class name
    for line_no in range(cursor[0]-1, -1, -1):
        line = vim.current.buffer[line_no]
        if not test_name and line.lstrip().startswith('def test'):
            test_name = re.findall('def (\w+)\(', line)[0]
        elif not test_name and line.lstrip().startswith('async def test'):
            test_name = re.findall('async def (\w+)\(', line)[0]
        if not class_name and line.startswith('class'):
            class_name = re.findall('class (\w+)\(', line)[0]
            break

    # " Example how VIM setting value can be used:
    # " run_py_test_format = vim.eval("get(g:, 'run_py_test_format', '')")
    # " if run_py_test_format == 'dotted':

    # " Generate test path for `manage.py test`
    test_path = '{test_filename}'.format(test_filename=test_filename)[:-3].replace('/', '.')
    if class_name:
        test_path += '.{class_name}'.format(class_name=class_name)
    if test_name:
        test_path += '.{test_name}'.format(test_name=test_name)

    # " Generate test path for `pytest`
    pytest_path = '{test_filename}'.format(test_filename=test_filename)
    if class_name:
        pytest_path += '::{class_name}'.format(class_name=class_name)
    if test_name:
        pytest_path += ' -k {test_name}'.format(test_name=test_name)

    print('\nRUN:', test_path, '\n')
    vim.command('let $TEST_ME_PLEASE="{test_path}"'.format(test_path=test_path))
    vim.command('let $PYTEST_ME_PLEASE="{pytest_path}"'.format(pytest_path=pytest_path))
    cmd = 'te TEST_ME_PLEASE="{test_path}" PYTEST_ME_PLEASE="{pytest_path}" make test'.format(test_path=test_path, pytest_path=pytest_path)
    vim.command(cmd)
    vim.command('redraw!')
else:
    vim.command('te make test')
    vim.command('redraw!')

EOF
endfunction


function! InsertBreakpoint()
python3 << EOF
# TODO: this function breaks if executed on empty file with IndexError
import re
import vim  # https://vimhelp.org/if_pyth.txt.html

cursor = vim.current.window.cursor

add_spaces = False
for i in range(20):
    line = vim.current.buffer[cursor[0] -1 - i]
    if line.strip():
        if line.strip().endswith(':'):
            add_spaces = True
        break
    line = vim.current.buffer[cursor[0] -1 + i]
    if line.strip():
        break

whitespace = re.findall('^(\s*)\S', line)
whitespace = whitespace[0] if whitespace else ''
if add_spaces:
    whitespace += '    '
vim.current.buffer.append(f'{whitespace}breakpoint()', cursor[0])

EOF
endfunction


function! FormatJson()
python << EOF
import vim
import json
try:
    buf = vim.current.buffer
    json_content = '\n'.join(buf[:])
    content = json.loads(json_content)
    sorted_content = json.dumps(content, indent=2, sort_keys=True)
    buf[:] = sorted_content.split('\n')
except Exception, e:
    print(e)
EOF
endfunction


command! DisableDiagnostics lua disable_diagnostics()
command! EnableDiagnostics lua enable_diagnostics()
]])

