-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here
--
--
--
-- vim.opt["mouse"] = "iv" -- modes in which mouse will work (disabled mouse to be able to select in term mode)
vim.opt["tabstop"] = 4
vim.opt["shiftwidth"] = 4
vim.opt["smartcase"] = false
vim.opt["smartindent"] = false
vim.opt["ignorecase"] = true

vim.g.file_line_crosshairs = 0

-- For some reason this mapping does not work
-- lvim.keys.normal_mode[";"] = "ysiw\""

-- Autocommands (https://neovim.io/doc/user/autocmd.html)
vim.api.nvim_create_autocmd("TermOpen", {
  pattern = { "*" },
  command = "startinsert",
})

-- jump to next diagnostic fast
vim.keymap.set("n", "[[", vim.diagnostic.goto_prev)
vim.keymap.set("n", "]]", vim.diagnostic.goto_next)

-- from https://www.youtube.com/watch?v=0moS8UHupGc&t=430s
vim.keymap.set("n", "<F5>", ':lua require"dap".continue()<CR>')
vim.keymap.set("n", "<F10>", ':lua require"dap".step_over()<CR>')
vim.keymap.set("n", "<F11>", ':lua require"dap".step_into()<CR>')
vim.keymap.set("n", "<F12>", ':lua require"dap".step_out()<CR>')
vim.keymap.set("n", "<leader>b", ':lua require"dap".toggle_breakpoint()<CR>')
vim.keymap.set("n", "<leader>B", ':lua require"dap".set_breakpoint(vim.fn.input("Breakpoint condition: "))<CR>')
vim.keymap.set("n", "<leader>lp", ':lua require"dap".set_breakpoint(nil, nil, vim.fn.input("Log point message: "))<CR>')
vim.keymap.set("n", "<leader>dr", ':lua require"dap".repl.open()<CR>')

-- Functions for interacting with diagnostics:
function _G.disable_diagnostics()
  vim.diagnostic.enable(false)
end
function _G.enable_diagnostics()
  vim.diagnostic.enable(true)
end

-- TODO: use this plugin for highlighting vimrc and python correctly: https://github.com/inkarkat/vim-SyntaxRange
vim.cmd([[
set rnu
map        \    gcc
vmap       \    gc
" nmap       ""   ysiW"
nmap       ""w   ysiw"
nmap       ""W   ysiW"
vmap       ""   S"
map        <leader>y    "*y
vmap       <leader>y    "*y
map        <leader>p    "*p
map        <leader>P    "*P
function! RunTest()
python3 << EOF
# CallMakeTestWithCurrentPythonTest()
# TODO: Should allow to somehow run pytest without arguments
import re
import os
import vim  # https://vimhelp.org/if_pyth.txt.html
import pynvim
cursor = vim.current.window.cursor
test_filename = vim.eval("expand('%p')")
if os.path.basename(test_filename).startswith('test'):
    test_name = None
    class_name = None
    # " Parse test name and its TestCase class name
    for line_no in range(cursor[0]-1, -1, -1):
        line = vim.current.buffer[line_no]
        if not test_name and line.lstrip().startswith('def test'):
            test_name = re.findall(r'def (\w+)\(', line)[0]
        elif not test_name and line.lstrip().startswith('async def test'):
            test_name = re.findall(r'async def (\w+)\(', line)[0]
        if not class_name and line.startswith('class'):
            class_name = re.findall(r'class (\w+)\(', line)[0]
            break
    # " Example how VIM setting value can be used:
    # " run_py_test_format = vim.eval("get(g:, 'run_py_test_format', '')")
    # " if run_py_test_format == 'dotted':
    # " Generate test path for `manage.py test`
    test_path = '{test_filename}'.format(test_filename=test_filename)[:-3].replace('/', '.')
    if class_name:
        test_path += '.{class_name}'.format(class_name=class_name)
    if test_name:
        test_path += '.{test_name}'.format(test_name=test_name)
    # " Generate test path for `pytest`
    pytest_path = '{test_filename}'.format(test_filename=test_filename)
    if class_name:
        pytest_path += '::{class_name}'.format(class_name=class_name)
    if test_name:
        pytest_path += ' -k {test_name}'.format(test_name=test_name)

    print('\nRUN:', test_path, '\n')
    vim.command('let $TEST_ME_PLEASE="{test_path}"'.format(test_path=test_path))
    vim.command('let $PYTEST_ME_PLEASE="{pytest_path}"'.format(pytest_path=pytest_path))
    vim.command('let $TEST_NAME="{pytest_path}"'.format(pytest_path=pytest_path))
    cmd = 'te TEST_ME_PLEASE="{test_path}" PYTEST_ME_PLEASE="{pytest_path}" TEST_NAME="{pytest_path}" make test'.format(test_path=test_path, pytest_path=pytest_path)
    vim.command(cmd)
    vim.command('redraw!')
else:
    vim.command('te make test')
    vim.command('redraw!')
EOF
endfunction
function! InsertBreakpoint()
python3 << EOF
# TODO: this function breaks if executed on empty file with IndexError
import re
import vim  # https://vimhelp.org/if_pyth.txt.html
cursor = vim.current.window.cursor
add_spaces = False
for i in range(20):
    line = vim.current.buffer[cursor[0] -1 - i]
    if line.strip():
        if line.strip().endswith(':'):
            add_spaces = True
        break
    line = vim.current.buffer[cursor[0] -1 + i]
    if line.strip():
        break
whitespace = re.findall(r'^(\s*)\S', line)
whitespace = whitespace[0] if whitespace else ''
if add_spaces:
    whitespace += '    '
vim.current.buffer.append(f'{whitespace}breakpoint()', cursor[0])
EOF
endfunction
function! FormatJson()
python << EOF
import vim
import json
try:
    buf = vim.current.buffer
    json_content = '\n'.join(buf[:])
    content = json.loads(json_content)
    sorted_content = json.dumps(content, indent=2, sort_keys=True)
    buf[:] = sorted_content.split('\n')
except Exception, e:
    print(e)
EOF
endfunction
command! DisableDiagnostics lua disable_diagnostics()
command! EnableDiagnostics lua enable_diagnostics()
]])
