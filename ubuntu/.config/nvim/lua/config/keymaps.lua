-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

local map = vim.keymap.set

map("n", "<C-s>", ":w<cr>", { desc = "Save" })
map("n", "<C-x>", ":x<cr>", { desc = "Save and Quit" })
map("n", "<C-q>", ":q<cr>", { desc = "Quit" })

map("n", "_", "@q", { desc = "Repeat macro" })
--  TODO define the same in comman line to avoid need to write norm

map("n", "<A-a>", "ea", { desc = "Insert in the end" })
map("n", "<A-A>", "Ea", { desc = "Insert in the end" })
map("n", "<A-i>", "bi", { desc = "Insert in the end" })
map("n", "<A-I>", "Bi", { desc = "Insert in the end" })
-- -- lvim.keys.normal_mode["\""] = "ysiW\""
--
map("n", "<leader>t", ":w<CR>:call RunTest()<CR>", { desc = "Run current test" })
map("n", "<leader>i", ":call InsertBreakpoint()<CR>:w<CR>", { desc = "Insert breakpoint" })
--
-- -- lvim.keys.normal_mode["<leader>b"]  = ":call CreateJIRABranchFromRegister()<CR>"
map("n", "<leader>a", "O<esc>:.! date '+\\%Y-\\%m-\\%d'<Enter>A[]<esc>hx^P<CR>", { desc = "Insert date" })
map("n", "<leader>da", "GVggxi", { desc = "Delete everything" })
