# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=
HISTFILESIZE=

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
xterm-color) color_prompt=yes ;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm* | rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*) ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -haltrF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

if [[ $(id -u) -ne 0 ]]; then
    PS1='\A \W$ '
else
    PS1='\A \W\[\033[0;32m\]$\[\033[00m\] '
fi

export TERM=xterm-256color
#export REUSE_DB=1
export REUSE_DB=0

export CLICOLOR='true'
export LSCOLORS="gxfxcxdxbxegedabagacad"
export PATH=$PATH:/opt/Espressif/crosstool-NG/builds/xtensa-lx106-elf/bin:/home/niekas/bin
export PATH="$PATH:/opt/nvim-linux64/bin"
export LD_LIBRARY_PATH=/usr/local/lib:${LD_LIBRARY_PATH}
export PATH=/home/niekas/nvim/bin/:/home/niekas/venv/bin/:$PATH

export EDITOR='nvim'
export AWS_XRAY_SDK_ENABLED=false
export NEW_YORK_TIMES_API_KEY=RlhGyIftoSKznUgBxxMJFB07wQBqmOQa

alias iv='nvim'
alias vi='nvim'
# alias y='yt-dlp'
alias y='yt-dlp -v -f "bv*[height<=720][ext=mp4][vcodec^=avc1]+ba*[ext=m4a]" -N 4'
# alias vim='lvim'
# alias nvim='lvim'

alias s.='s .venv/bin/activate'
alias changes="git dc | colordiff"
alias ls='ls --hide="*.pyc" --color=auto'
alias open=xdg-open
alias m='vim ~/me.rst'
alias e='vim ~/verslas.rst'
alias n='vim ~/notes.rst'
alias i='vim ~/learn/ideas.rst'
alias a='vi ~/action_items.rst'
# alias d='vim ~/diary.rst'
alias w='vi ~/learn/words'
alias t='vim ~/todo.rst'
alias j='vim ~/job.rst'
alias f='vim ~/free_time_todo.rst'
alias p='ipython'
# alias e='vi ~/teach/eva'
source '/usr/share/bash-completion/completions/git'
alias g='git'
__git_complete g __git_main
alias gi='git'
alias ig='git'
alias c='make check'
alias blender='/home/strazdas/blender/blender'
alias tox='/home/strazdas/qTox/qtox &'
alias dice='~/./roll_dice.py'
alias ..='cd ..'
alias cd..='cd ..'
alias smake='snakemake'
alias git-rm-merged='git ch main && git branch --merged | egrep -v "(^\*|master|main|dev)" | xargs git branch -d'
alias gg='git grep'
alias ga='git add'
alias gb='git add'
alias gst='git status'
alias gp='git push'
alias gpf='git push --force-with-lease'
alias gd='git diff'
alias dg='git diff'
alias gdc='git diff --staged'
alias gc='git checkout'
__git_complete ga _git_add
__git_complete gc _git_checkout
__git_complete gp _git_push
__git_complete gpf _git_push
__git_complete gb _git_branch

alias st='git status'
alias lg='git lg'
# alias ytd='yt-dlp -f "bv*[ext=mp4]+ba[ext=m4a]/b[ext=mp4] / bv*+ba/b"'
alias ytd='/home/niekas/youtube-dl -f "bestvideo[ext!=webm]+bestaudio[ext!=webm]/best[ext!=webm] / bv*+ba/b"'

alias python='python3'
alias path='readlink -f'
alias s='source'
alias mp='/home/niekas/venv/bin//modernpackage'

# alias a='source env/bin/activate'
# alias python3.9='/home/niekas/.pyenv/versions/3.9.2/bin/python3.9'
# ln -s /home/niekas/.pyenv/versions/3.9.2/bin/python3.9 python3.9 # instead
alias python3.10='/home/niekas/.pyenv/versions/3.10.12/bin/python3.10'
alias python3.12='/home/niekas/.pyenv/versions/3.12.2/bin/python3.12'
# alias python3.12='/home/niekas/.pyenv/versions/3.10.12/bin/python3.10'
# alias python3.11='/home/niekas/.pyenv/versions/3.11.7/bin/python3.7'
# ln -s /home/niekas/.pyenv/versions/3.10.12/bin/python3.10 python3.10
# ln -s /home/niekas/.pyenv/versions/3.12.2/bin/python3.12 python3.12

alias slides='lookatme --no-ext-warn'
alias blender='~/blender/blender'

export HATCH_INDEX_USER=__token__
export HATCH_INDEX_AUTH=pypi-AgEIcH
# pypi-AgEIcHlwaS5vcmcCJDY3NWQ2MmNmLTNjMDgtNGUzNy1hMjg3LTBjM2M3MTBhMjYxMQACEFsxLFsiZGVhZGNvZGUiXV0AAixbMixbImE4ZjY4ZjkyLTQxZWMtNGNjNi1hYzhhLTcwZjc1NGYxYzhhZCJdXQAABiCopUx-Q9ZDoQ_pr0thTK9aKpG7F1R9a99FCPJocj85ag

# FZF set better find file tool (e.g. ripgrep, which respects .gitignore)
# sudo apt install ripgrep
if type ag &>/dev/null; then
    export FZF_DEFAULT_COMMAND='ag -p ~/.gitignore -g ""'
fi
#refer rg over ag
if type rg &>/dev/null; then
    export FZF_DEFAULT_COMMAND='rg --files --hidden'
fi

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"                   # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # This loads nvm bash_completion

# Temporarily for testing purposes
export GITLAB_ACCESS_TOKEN=glpat-8Y1Y
# export GITHUB_OAUTH_TOKEN=ghp_uZvwZ
export GITHUB_OAUTH_TOKEN=ghp_fdd

export OPENAI_API_KEY=sk-tjGzl

# pnpm
export PNPM_HOME="/home/niekas/.local/share/pnpm"
case ":$PATH:" in
*":$PNPM_HOME:"*) ;;
*) export PATH="$PNPM_HOME:$PATH" ;;
esac
# pnpm end
# . "$HOME/.cargo/env"
