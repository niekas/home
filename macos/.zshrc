[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

. "$HOME/.cargo/env"
export PATH=/Users/albertasgimbutas/.local/bin:$PATH
alias vi=nvim


# alias iv='lvim'
# alias vi='lvim'
# alias vim='lvim'
# alias nvim='lvim'

alias changes="git dc | colordiff"
alias open=xdg-open
alias m='vi ~/me.rst'
alias n='vi ~/notes'
alias d='vi ~/diary.rst'
alias t='vi ~/todo.rst'
alias a='vi ~/action_items'
alias p='ipython'
# source '/usr/share/bash-completion/completions/git'
alias g='git'
# __git_complete g __git_main
alias gi='git'
alias ig='git'
alias c='make check'
alias ..='cd ..'
alias cd..='cd ..'
alias gg='git grep'
alias ga='git add'
alias gb='git add'
alias gst='git status'
alias gp='git push'
alias gpf='git push --force-with-lease'
alias gd='git diff'
alias dg='git diff'
alias gdc='git diff --staged'
alias gc='git checkout'
alias rmmerged='git branch --merged | grep -Ev "(^\*|master|main|dev|devel)" | xargs git branch -d'
# __git_complete ga _git_add
# __git_complete gc _git_checkout
# __git_complete gp _git_push
# __git_complete gpf _git_push
# __git_complete gb _git_branch

alias st='git status'
alias push='git push'
alias lg='git lg'
alias s='source'
alias s.='source .venv/bin/activate'

export EDITOR=nvim

alias ip='ipython'

alias ls='ls -G'
export CLICOLOR=1
export LSCOLORS=gxFxCxDxBxegedabagaced

export SLACK_BOT_TOKEN='xoxb-2151195509-7865536310151-b34lwkvveEoBph8qZOL0EUTy'
export SLACK_SIGNING_SECRET='3e9b3b205a257f82ab83f61ad1d39c50'
export CROWDIN_API_TOKEN='eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiJ3UUVxdmhVM3ZMT2EyWGljbVV5VCIsImp0aSI6IjVjODU1ZjVlOWFhMDJlMjBlMTFmOWNlMjNlZjU5OTBmOGI2NDRhNDNjZjJjYjQ5MDQxODJhODIyNzBkYWNjMzYyNDkzNWQyMjg0ODBjMmUwIiwiaWF0IjoxNzMwOTgzNzg1LjY4OTkxOCwibmJmIjoxNzMwOTgzNzg1LjY4OTkyMSwiZXhwIjoxNzYyNTE5Nzg1LjYwNjgyMSwic3ViIjoiMTY2NjQ4NDMiLCJzY29wZXMiOlsicHJvamVjdCJdLCJkb21haW4iOm51bGwsImFzc29jaWF0aW9ucyI6WyIqIl0sImxvZ2luX21ldGhvZCI6bnVsbCwic2Vzc2lvbiI6MH0.N0sebCLNkDBysBddswQcM6bgrYgpfJs7wCudQkIQi-6AI-uB8b6psrgJwb1aly_V-E4HWU6e96AvL7psL4jo6RFv_r3k3tFCqIpil1yGWRZWDad4_Z4H57IOegMyn_Tua74929EXeeMxxfC-cGcOHp1bKD_ddn_qM1dWRU1QaNgkFhwl4VnabotZkQBablVzx2WNA6TYnAq763YQITGVLsxk6ldT_qzk5sRg9rKVqaOykvaXT0s1MbMF0ytlxJmiOjo6kR6gQVHBlppc2vZ8-Q5Kz6T9DBoLEGSh00oRNh5x4PtiBdomuU8axN-X9yInh2xllYC8WGDVjkSvdpz4XWS2fTlx4QW3k9kq_IfyL_Xg85YnO37woEsdMWJWMWwPMkjg5RbpakTjBb_zo7rRDt0PAVNRrjvsFymkpR3UiE4yQm523D62IcaV5T44KxoaAR1AWTwSzu5n03jiFRIdi40_lhWrkY_fGnxdQKiWZyUv0XGBIO8_XVJKpfylO4TPtlG58X1o8Fq8UvSIVRpnBA3YEI_wRVrPHrFa4n32fcrpvEk7I3E7hMguIeD3ZXRuiDrQoDuNUdEq96d76CQ-LjVgNkZ7lYfCdKeWOc5_r5GJ8E0YWztQMLoV582155fJGQn_uLrniYNRKaCM271CO4X8e0Sq4Of1LephmcWCEQk'

alias gr="grep --color -Rnw . -e"
