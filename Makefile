# .ONESHELL:
# SHELL := /bin/bash  # Allows to cd/source or similar command to run in Makefile 

copy_config_files:

	mkdir -p ~/.config/lvim
	cp config.lua ~/.config/lvim/config.lua

	# TODO: Use bash conditions not to ask for sudo if everything is installed or already in sudo:
	# 	https://tldp.org/LDP/Bash-Beginners-Guide/html/sect_07_01.html
	# local is_alpine=$(command -v apk)
	# if [[ -z is_alpine ]]; then
	# sudo pip3 install neovim  # This step is needed when working in virtual environment
	@echo "Installing pip packages"
	pip3 install pip --upgrade
	pip3 install pynvim --upgrade
	sudo apt-get install git git-core vim vim-nox neovim python3-neovim curl || true
	pip3 install black flake8 flake8 flake8-import-order flake8-blind-except flake8-django flake8-bugbear flake8-type-annotations \
					 pep8-naming flake8-builtins flake8-logging-format flake8-variables-names flake8-functions flake8-comprehensions \
					 flake8-bandit django-stubs mypy flake8-print flakehell isort || true  # flake8-isort flake8-black 


	# Config file is:
	# ~/.config/lvim/config.lua

	# else # command exists
	# 	apk add make git vim neovim curl python3-dev g++ neovim-doc 
	# 	sudo pip nvim install black flake8 flake8 flake8-import-order flake8-blind-except flake8-django flake8-bugbear flake8-type-annotations \
	# 					 pep8-naming flake8-builtins flake8-logging-format flake8-variables-names flake8-functions flake8-comprehensions \
	# 					 flake8-bandit django-stubs mypy flake8-print flakehell isort || true  # flake8-isort flake8-black 
	# fi

	cp .bashrc ~/
	cp .gitconfig ~/
	cp flake8 ~/.config/flake8
	# sh -c 'curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
	#
	# TODO: add automatic NeoVIM installation instructions.
	#
	#### Install NeoVIM
	# $ sudo add-apt-repository ppa:neovim-ppa/stable   # unstable
	# $ sudo apt-get update
	# $ sudo apt-get install neovim
	# $ sudo apt remove neovim -y
	#
	# sudo snap find nvim
        sudo snap install nvim --classic


	## Old vim config:
	# mkdir -p ~/.config/nvim
	# cp .vimrc ~/.config/nvim/init.vim
	# cp -r .vim/* ~/.config/nvim
	# mkdir -p ~/.vim
	# # cp .vimrc ~/.vimrc
	# cp -r .vim/* ~/.vim

install_neovim_and_lunar_vim:
	# Install rust and nodejs
	curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
	source ~/.bashrc
	nvm install --lts
	npm install cargo


	@echo "Install NeoVim"
	sudo add-apt-repository ppa:neovim-ppa/unstable   # stable
	sudo apt-get update
	sudo apt-get install neovim
	# $ sudo apt remove neovim -y
	#
	@echo "Installing LunvarVim"
	LV_BRANCH='release-1.2/neovim-0.8' bash <(curl -s https://raw.githubusercontent.com/lunarvim/lunarvim/fc6873809934917b470bff1b072171879899a36b/utils/installer/install.sh)
